package com.prep.parkinglot.services;


import com.prep.parkinglot.models.Ticket;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PricingService {

    public double calculateFare(Ticket ticket) {
        double fare = 20.0; // base fare
        Date exitTime = new Date();
        Date entryTime = ticket.getEntryTime();


        long time_difference = exitTime.getTime() - entryTime.getTime();
        long days_difference = (time_difference / (1000*60*60*24)) % 365;
//        long years_difference = (time_difference / (1000l*60*60*24*365));
//        long seconds_difference = (time_difference / 1000)% 60;
//        long minutes_difference = (time_difference / (1000*60)) % 60;
        long hours_difference = (time_difference / (1000*60*60)) % 24;


        if(hours_difference < 1 && days_difference < 1) return fare; // base fare
        if(days_difference > 0) fare += days_difference * 450.0;
        if(hours_difference > 0) fare += hours_difference * 20.0;
        return fare;
    }
}
