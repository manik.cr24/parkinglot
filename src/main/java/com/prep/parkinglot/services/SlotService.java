package com.prep.parkinglot.services;


import com.prep.parkinglot.enums.VehicleType;
import com.prep.parkinglot.exceptions.SlotNotAvailableException;
import com.prep.parkinglot.models.Slot;
import com.prep.parkinglot.repo.SlotRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlotService {

    @Autowired
    private SlotRepo slotRepo;

    public Slot allocateSlot(VehicleType vehicleType) {
//        Slot availableSlot = slotRepo.findAvailableSlot(vehicleType);
//        List<Slot> slots = slotRepo.findAvailableSlot(getVehicleType(vehicleType));
//        List<Slot> slots = slotRepo.findAvailableSlot((vehicleType));
        List<Slot> slots = slotRepo.findByIsOccupiedAndVehicleType(false, vehicleType);
        if(slots.isEmpty()) throw new SlotNotAvailableException();

        Slot availableSlot = slots.get(0);
        availableSlot.setOccupied(true);
        return slotRepo.save(availableSlot);
    }

    private int getVehicleType(VehicleType vehicleType) {
        if(vehicleType == VehicleType.SMALL) return 0;
        if(vehicleType == VehicleType.MEDIUM) return 1;
        return 2;
    }

    public boolean unallocateSlot(long slotId) {
        Slot slot = slotRepo.findById(slotId).get();
        slot.setOccupied(false);
        slot = slotRepo.save(slot);
        if(slot != null) return true;
        return false;
    }
}
