package com.prep.parkinglot.services;

import com.prep.parkinglot.enums.PaymentStatus;
import com.prep.parkinglot.enums.TicketStatus;
import com.prep.parkinglot.models.Ticket;
import com.prep.parkinglot.repo.TicketRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TicketService {

    @Autowired
    private TicketRepo ticketRepo;


    public Ticket getTicket(long ticketId) {
        return ticketRepo.findById(ticketId).orElse(null);
    }

    public Ticket createTicket(Long slotId, String vehicleNumber) {
        Ticket ticket = new Ticket();
        ticket.setStatus(TicketStatus.OPEN);
        ticket.setEntryTime(new Date());
        ticket.setVehicleNumber(vehicleNumber);
        ticket.setSlotId(slotId);

        Ticket savedTicket = ticketRepo.save(ticket);
        return savedTicket;
    }

    public boolean closeTicket(Ticket ticket, Date exitTime, double amount, PaymentStatus paymentStatus) {
        ticket.setExitTime(exitTime);
        ticket.setAmount(amount);
        ticket.setPaymentStatus(paymentStatus);

        Ticket save = ticketRepo.save(ticket);
        if(save != null) return true;
        return false;
    }
}
