package com.prep.parkinglot.services;


import com.prep.parkinglot.enums.PaymentStatus;
import com.prep.parkinglot.enums.TicketStatus;
import com.prep.parkinglot.enums.VehicleType;
import com.prep.parkinglot.exceptions.TicketNotValidException;
import com.prep.parkinglot.models.Slot;
import com.prep.parkinglot.models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ParkingService {

    @Autowired
    private SlotService slotService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private PricingService pricingService;

    @Autowired
    private PaymentService paymentService;


    public Ticket entry(VehicleType vehicleType, String vehicleNumber) {
        Slot allocatedSlot = slotService.allocateSlot(vehicleType);
        Ticket ticket = ticketService.createTicket(allocatedSlot.getId(), vehicleNumber);
        return ticket;

    }

    public ResponseEntity exit(long ticketId) {
        Date exitTime = new Date();
        Ticket ticket = ticketService.getTicket(ticketId);
        if(ticket == null) throw new TicketNotValidException("This ticket doesn't exist in our system");
        if(ticket.getStatus() == TicketStatus.CLOSED) throw new TicketNotValidException("This ticket was already closed at "+ticket.getExitTime());

        Long slotId = ticket.getSlotId();

        slotService.unallocateSlot(slotId);
        double amount = pricingService.calculateFare(ticket);
        PaymentStatus paymentStatus = paymentService.processPayment(ticketId, amount);
        ticketService.closeTicket(ticket, exitTime, amount, paymentStatus);
        return new ResponseEntity(HttpStatus.OK);
    }

}
