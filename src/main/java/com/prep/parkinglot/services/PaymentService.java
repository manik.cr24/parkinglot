package com.prep.parkinglot.services;

import com.prep.parkinglot.enums.PaymentStatus;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    public PaymentStatus processPayment(Long ticketId, double amount) {
        // logic to handle payment
        return PaymentStatus.PAID;
    }
}
