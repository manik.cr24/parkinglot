package com.prep.parkinglot.models;

import com.prep.parkinglot.enums.PaymentStatus;
import com.prep.parkinglot.enums.TicketStatus;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    Long id;

    @Column
    TicketStatus status;
    @Column
    Date entryTime;

    @Column
    @Nullable
    Date exitTime;

    @Column
    String vehicleNumber;

    @Column
    Long slotId;

    @Column
    Double amount;

    @Column
    PaymentStatus paymentStatus;
}
