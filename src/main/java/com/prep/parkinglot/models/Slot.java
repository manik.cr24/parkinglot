package com.prep.parkinglot.models;



import com.prep.parkinglot.enums.VehicleType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "slot")
public class Slot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "is_occupied")
    private Boolean isOccupied;

    @Column(name = "vehicle_type")
    @Enumerated
    private VehicleType vehicleType;


    public Boolean getOccupied() {
        return isOccupied;
    }

    public void setOccupied(Boolean occupied) {
        isOccupied = occupied;
    }


    public Long getId() {
        return id;
    }
}
