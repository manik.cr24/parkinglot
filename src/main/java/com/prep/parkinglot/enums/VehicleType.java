package com.prep.parkinglot.enums;

public enum VehicleType {
    SMALL,
    MEDIUM,
    LARGE
}
