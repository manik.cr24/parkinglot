package com.prep.parkinglot.enums;

public enum PaymentStatus {
    PAID,
    NOT_PAID
}
