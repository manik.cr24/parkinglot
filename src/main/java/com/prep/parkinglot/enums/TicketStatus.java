package com.prep.parkinglot.enums;

public enum TicketStatus {
    OPEN, CLOSED
}
