package com.prep.parkinglot.exceptions;

public class TicketNotValidException extends RuntimeException{
    public TicketNotValidException(String message) {
        super(message);
    }
}
