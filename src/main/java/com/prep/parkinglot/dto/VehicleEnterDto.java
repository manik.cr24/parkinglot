package com.prep.parkinglot.dto;

import com.prep.parkinglot.enums.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleEnterDto {
    private String vehicleNumber;
    private VehicleType vehicleType;
}
