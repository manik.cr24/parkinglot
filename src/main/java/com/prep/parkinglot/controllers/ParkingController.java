package com.prep.parkinglot.controllers;


import com.prep.parkinglot.dto.VehicleEnterDto;
import com.prep.parkinglot.dto.VehicleExitDto;
import com.prep.parkinglot.exceptions.SlotNotAvailableException;
import com.prep.parkinglot.exceptions.TicketNotValidException;
import com.prep.parkinglot.models.Ticket;
import com.prep.parkinglot.services.ParkingService;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
public class ParkingController {

    private static final Logger logger = LoggerFactory.getLogger(ParkingController.class);

    @Autowired
    private ParkingService parkingService;

    @PostMapping(value = "/enter", consumes = "application/json")
    public Ticket enter(@RequestBody VehicleEnterDto vehicleInfo) {
        Ticket ticket = parkingService.entry(vehicleInfo.getVehicleType(), vehicleInfo.getVehicleNumber());
        return ticket;
    }

    @PostMapping(value = "/exit", consumes = "application/json")
    public ResponseEntity exit(@RequestBody VehicleExitDto vehicleInfo) {
        return parkingService.exit(vehicleInfo.getTicketId());
    }


    @ExceptionHandler({ TicketNotValidException.class })
    public void handleTicketNotValid(TicketNotValidException exception, HttpServletResponse response) throws IOException {
        logger.error("Invalid ticket id", exception);
        response.setStatus(404);
        response.getWriter().write("This ticket is not valid");
    }
    @ExceptionHandler({ SlotNotAvailableException.class })
    public void handleException(SlotNotAvailableException ex, HttpServletResponse response) throws IOException {
        logger.error("Slot is not available", ex);
        response.setStatus(410);
        response.getWriter().write("No slot available at this moment");
    }
}
