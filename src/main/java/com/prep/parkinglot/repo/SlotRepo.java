package com.prep.parkinglot.repo;


import com.prep.parkinglot.enums.VehicleType;
import com.prep.parkinglot.models.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SlotRepo extends JpaRepository<Slot, Long> {

//    @Query("select slt from slot slt where slt.isOccupied=false and slt.vehicleType=:vehicleType")
//    List<Slot> findAvailableSlot(@Param("vehicleType") VehicleType vehicleType);

    List<Slot> findByIsOccupiedAndVehicleType(Boolean isOccupied, VehicleType vehicleType);
}
